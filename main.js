/*jslint browser:true */
/*global $, moment, _, Set, Backbone, token, Highcharts */

/**
 * Menu déroulant
 */
var ComboBox = Backbone.View.extend({

    initialize: function(options) {
        if (options.hasOwnProperty('values')) {
            this.set(options.values);
        }
    },

    events: {
        'click li': 'onClick',
    },

    set: function(items) {
        var $list = this.$el.find('ul'),
            entry, i;
        $list.empty();
        this.$el.find('.menu-title').html('');
        for (i = 0; i < items.length; i++) {
            entry = $('<li><a href="#">' + items[i].title + '</li>');
            entry.data('name', items[i].name);
            $list.append(entry);
        }
    },

    onClick: function(event) {
        var item = $(event.currentTarget);
        this.select(item);
    },

    selectFirst: function() {
        this.select($(this.$el.find('li').get(0)));
    },

    select: function(item) {
        var name = item.data('name'),
            title = item.find('a').html();
        this.$el.find('.menu-highlight').removeClass('menu-highlight');
        item.addClass('menu-highlight');
        this.$el.find('.menu-title').html('(' + title + ')');
        this.trigger('menu-select', name);
    },

    getValue: function() {
        return this.$el.find('.menu-highlight').data('name');
    },
});


/**
 * Vue d'un graphique
 */
var ChartView = Backbone.View.extend({

    initialize: function(options) {
        var tmpl = _.template($('#chart-template').html());
        this.$el.html(
            tmpl({
                url: options.url
            })
        );
        $('#charts-view').append(this.$el);
        this.resize();
    },

    remove: function() {
        this.$el.remove();
        this.$el = null;
    },

    resize: function() {
        this.$el.find('img').width(this.$el.find('.well').width());
    },
});


/**
 * Vue principale
 */
var MainView = Backbone.View.extend({

    menu: [
        {
            name: 'beta',
            title: 'Beta',
            items: [
                {
                    name: 'shape10.5_shape20.5',
                    title: 'alpha=0.5, beta=0.5',
                },
                {
                    name: 'shape15_shape21',
                    title: 'alpha=5, beta=1',
                },
                {
                    name: 'shape11_shape23',
                    title: 'alpha=1, beta=3',
                },
                {
                    name: 'shape12_shape22',
                    title: 'alpha=5, beta=2',
                },
                {
                    name: 'shape12_shape25',
                    title: 'alpha=2, beta=5',
                }
            ]
        },
        {
            name: 'gamma',
            title: 'Gamma',
            items: [
                {
                    name: 'shape1_rate0.5',
                    title: 'shape=1, rate=0.5',
                },
                {
                    name: 'shape2_rate0.5',
                    title: 'shape=2, rate=0.5',
                },
                {
                    name: 'shape3_rate0.5',
                    title: 'shape=3, rate=0.5',
                },
                {
                    name: 'shape5_rate1',
                    title: 'shape=5, rate=1',
                },
                {
                    name: 'shape9_rate2',
                    title: 'shape=9, rate=2',
                },
                {
                    name: 'shape7.5_rate1',
                    title: 'shape=7.5, rate=1',
                },
                {
                    name: 'shape0.5_rate1',
                    title: 'shape=0.5, rate=1',
                }

            ]
        },
        {
            name: 'norMix',
            title: 'Gaussian mixture',
            items: [
                {
                    name: '',
                    title: 'no choice',
                }
            ]
        },
        // {
        //     name: 'gev',
        //     title: 'GEV',
        //     items: [
        //         {
        //             name: 'loc0_scale1_shape1',
        //             title: 'mu=0, sigma=1, xi=1',
        //         },
        //         {
        //             name: 'loc0_scale1_shape5',
        //             title: 'mu=0, sigma=1, xi=5',
        //         },
        //         {
        //             name: 'loc0_scale1_shape10',
        //             title: 'mu=0, sigma=1, xi=10',
        //         },
        //         {
        //             name: 'loc0_scale2_shape1',
        //             title: 'mu=0, sigma=2, xi=1',
        //         },
        //         {
        //             name: 'loc0_scale2_shape5',
        //             title: 'mu=0, sigma=2, xi=5',
        //         },
        //         {
        //             name: 'loc0_scale2_shape20',
        //             title: 'mu=0, sigma=2, xi=20',
        //         }
        //     ]
        // },
        // {
        //     name: 'gpd',
        //     title: 'Generalized Pareto',
        //     items: [
        //         {
        //             name: 'loc0_scale1_shape1',
        //             title: 'mu=0, sigma=1, xi=1',
        //         },
        //         {
        //             name: 'loc0_scale1_shape5',
        //             title: 'mu=0, sigma=1, xi=5',
        //         },
        //         {
        //             name: 'loc0_scale1_shape10',
        //             title: 'mu=0, sigma=1, xi=10',
        //         },
        //         {
        //             name: 'loc0_scale2_shape1',
        //             title: 'mu=0, sigma=2, xi=1',
        //         },
        //         {
        //             name: 'loc0_scale2_shape5',
        //             title: 'mu=0, sigma=2, xi=5',
        //         },
        //         {
        //             name: 'loc0_scale2_shape20',
        //             title: 'mu=0, sigma=2, xi=20',
        //         }
        //     ]
        // },
        {
            name: 'lnorm',
            title: 'Log-normal',
            items: [
                {
                    name: 'm0_s0.25',
                    title: 'mu=0, sigma=0.25',
                },
                {
                    name: 'm0_s0.5',
                    title: 'mu=0, sigma=0.5',
                },
                {
                    name: 'm0_s1',
                    title: 'mu=0, sigma=1',
                }
            ]
        },
        {
            name: 'norm',
            title: 'Normal',
            items: [
                {
                    name: 'm0_s0.2',
                    title: 'mu=0, sigma=0.2',
                },
                {
                    name: 'm0_s1',
                    title: 'mu=0, sigma=1',
                },
                {
                    name: 'm0_s5',
                    title: 'mu=0, sigma=5',
                },
                {
                    name: 'm-2_s0.5',
                    title: 'mu=-2, sigma=0.5',
                }
            ]
        },
        {
            name: 'sqtruncnorm',
            title: 'Square-root truncated normal',
            items: [
                {
                    name: 'm2_s2_a0',
                    title: 'mu=2, sigma=2',
                },
                {
                    name: 'm10_s2_a0',
                    title: 'mu=10, sigma=2',
                },
                {
                    name: 'm19_s10_a0',
                    title: 'mu=19, sigma=10',
                },
                {
                    name: 'm0_s10_a0',
                    title: 'mu=0, sigma=10',
                }
            ]
        },
        {
            name: 'truncnorm',
            title: 'Truncated normal',
            items: [
                {
                    name: 'm2_s2_a0',
                    title: 'mu=2, sigma=2',
                },
                {
                    name: 'm10_s2_a0',
                    title: 'mu=10, sigma=2',
                },
                {
                    name: 'm19_s10_a0',
                    title: 'mu=19, sigma=10',
                },
                {
                    name: 'm0_s10_a0',
                    title: 'mu=0, sigma=10',
                }
            ]
        },
        {
            name: 'unif',
            title: 'Uniform',
            items: [
                {
                    name: 'min0_max1',
                    title: 'mini=0, maxi=1',
                }
            ]
        }
    ],

    prefix: 'convergence_crps_4intervals',
    columns: ['Ntx10', 'Ntx20', 'Ntx30', 'Ntx100'],
    // columns: ['Ntx10', 'Ntx20', 'Ntx30', 'Ntx100'],
    rows: ['_', 'xaeq_', 'xaeq_linjitter', 'xaeq_fulljitter'],

    initialize: function() {
        var menu1_values = [],
            name,
            view = this;
        for (name in this.menu) {
            if (this.menu.hasOwnProperty(name)) {
                menu1_values.push(name);
            }
        }
        this.menu1 = new ComboBox({
            el: $('#menu1-view'),
            values: this.menu,
        });
        this.menu2 = new ComboBox({
            el: $('#menu2-view')
        });
        this.menu3 = new ComboBox({
            el: $('#menu3-view')
        });

        this.chartsView = [];

        // Connection des évènements
        this.listenTo(this.menu1, 'menu-select', this.menu1Change);
        this.listenTo(this.menu2, 'menu-select', this.menu2Change);
        this.listenTo(this.menu3, 'menu-select', this.menu3Change);

        // Sélection automatique du premier item de menu
        this.menu1.selectFirst();

        // Callback de redimensionnent des images
        window.onresize = function(event) {
            view.resize();
        };
    },

    menu1Change: function(name) {
        var menu = this.menu,
            i;
        for (i = 0; i < menu.length; i++) {
            if (menu[i].name == name) {
                this.menu2.set(menu[i].items);
                this.menu3.set(menu[i].items);
                break;
            }
        }
        this.menu2.selectFirst();
        this.menu3.selectFirst();
    },

    menu2Change: function(name) {
        var distrib = this.menu1.getValue(),
            options2 = name,
            options3 = this.menu3.getValue(),
            i, j, url;
        this.clearCharts();
        for (i = 0; i < this.rows.length; i++) {
            for (j = 0; j < this.columns.length; j++) {
                url = distrib + '/' + this.prefix + '_' + distrib + '_' + this.rows[i] + '_'
                    + options2 + '_' + distrib + '_' + options3 + '_' + this.columns[j] + '.svg';
                this.chartsView.push(new ChartView({url: url}));
            }
        }
    },

    menu3Change: function(name) {
        var distrib = this.menu1.getValue(),
            options2 = this.menu2.getValue(),
            options3 = name,
            i, j, url;
        this.clearCharts();
        for (i = 0; i < this.rows.length; i++) {
            for (j = 0; j < this.columns.length; j++) {
                url = distrib + '/' + this.prefix + '_' + distrib + '_' + this.rows[i] + '_'
                    + options2 + '_' + distrib + '_' + options3 + '_' + this.columns[j] + '.svg';
                this.chartsView.push(new ChartView({url: url}));
            }
        }
    },

    clearCharts: function() {
        var i;
        for (i = 0; i < this.chartsView.length; i++) {
            this.chartsView[i].remove();
        }
        this.chartsView = [];
    },

    resize: function() {
        var i;
        for (i = 0; i < this.chartsView.length; i++) {
            this.chartsView[i].resize();
        }
    },
});
